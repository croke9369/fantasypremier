﻿using FantasyPremierClient.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FantasyPremierClient
{
    public interface IFantasyTeamHistory
    {
        Task<FantasyTeamHistory> GetHistory(int teamId);
    }
}
