﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace FantasyPremierClient.Data
{
    public class FantasyTeamHistory
    {
        [JsonProperty("chips")]
        public ICollection<FantasyTeamHistoryChip> Chips { get; set; }

        [JsonProperty("past")]
        public ICollection<FantasyTeamSeasonHistory> SeasonHistory { get; set; }

        [JsonProperty("current")]
        public ICollection<FantasyEventEntryHistory> GameweekHistory { get; set; }
    }
}
