﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace FantasyPremierClient.Data
{
    public class FantasyClassicLeague
    {
        [JsonProperty("new_entries")]
        public FantasyNewLeagueEntries NewEntries { get; set; }

        [JsonProperty("league")]
        public FantasyClassicLeagueProperties Properties { get; set; }

        [JsonProperty("standings")]
        public FantasyClassicLeagueStandings Standings { get; set; }
    }
}
