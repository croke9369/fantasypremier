﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace FantasyPremierClient.Data
{
    public class FantasyClassicLeagueStandings
    {
        [JsonProperty("has_next")]
        public bool HasNext { get; set; }

        [JsonProperty("number")]
        public int Number { get; set; }

        [JsonProperty("results")]
        public ICollection<FantasyClassicLeagueEntry> Entries { get; set; }
    }
}