﻿using FantasyPremierClient.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FantasyPremierClient.Clients
{
    public class FantasyTeamHistoryClient : IFantasyTeamHistory
    {
        private readonly HttpClient _client;

        public FantasyTeamHistoryClient(HttpClient client)
        {
            _client = client;
        }

        public async Task<FantasyTeamHistory> GetHistory(int teamId)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            var url = HistoryUrlFor(teamId);

            var json = await _client.GetStringAsync(url);

            return JsonConvert.DeserializeObject<FantasyTeamHistory>(json);
        }

        private static string HistoryUrlFor(int teamId)
        {
            return $"https://fantasy.premierleague.com/api/entry/{teamId}/history/";
        }
    }
}
