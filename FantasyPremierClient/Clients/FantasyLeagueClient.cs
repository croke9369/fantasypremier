﻿using FantasyPremierClient.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FantasyPremierClient.Clients
{
    public class FantasyLeagueClient : IFantasyLeagueClient
    {
        private readonly HttpClient _client;

        public FantasyLeagueClient(HttpClient client)
        {
            _client = client;
        }

        public async Task<FantasyClassicLeague> GetClassicLeague(int leagueId, int page = 1)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var url = ClassicLeagueUrlFor(leagueId, page);
            //_client.DefaultRequestHeaders.Add("csrftoken", "r7XpUsGB6h5RTIzMzcrth4CexljXSnQau2ez6P0MA8vVTE2UKnUxIWelbemEp7xc");
            _client.DefaultRequestHeaders.Add("Cookie", "pl_profile=\"eyJzIjogIld6SXNNVGc1TlRRd016bGQ6MWk3R0Q3OkNCdy0zODMyWHN6QzhYZEMxMFFCV3hFRThPVSIsICJ1IjogeyJpZCI6IDE4OTU0MDM5LCAiZm4iOiAiQ29ub3IiLCAibG4iOiAiQ3Jva2UiLCAiZmMiOiA2fX0=\"");

            var json = await _client.GetStringAsync(url);
            return JsonConvert.DeserializeObject<FantasyClassicLeague>(json);
        }

        private static string ClassicLeagueUrlFor(int leagueId, int? page)
        {
            var baseUrl = $"http://fantasy.premierleague.com/api/leagues-classic/{leagueId}/standings/";
            var suffix = $"?page_new_entries={page ?? 1}&page_standings={page ?? 1}";
            return $"{baseUrl}{suffix}";
        }

        private static string HeadToHeadLeagueUrlFor(int leagueId, int? page)
        {
            var baseUrl = $"http://fantasy.premierleague.com/api/leagues-h2h/{leagueId}/standings/";

            var suffix = $"?page_new_entries={page ?? 1}&page_standings={page ?? 1}";

            return $"{baseUrl}{suffix}";
        }
    }
}
