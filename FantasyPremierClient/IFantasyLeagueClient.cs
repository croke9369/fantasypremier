﻿using FantasyPremierClient.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FantasyPremierClient
{
    public interface IFantasyLeagueClient
    {
        Task<FantasyClassicLeague> GetClassicLeague(int leagueId, int page = 1);
    }
}
