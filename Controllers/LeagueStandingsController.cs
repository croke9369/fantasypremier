﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FantasyPremierClient;
using FantasyPremierClient.Clients;
using FantasyPremierClient.Data;

namespace FPL.Controllers
{
    public class LeagueStandingsController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task <IActionResult> GetLeague(int leagueId)
        {
            ViewBag.LeagueId = leagueId;

            var client = new FantasyLeagueClient(new HttpClient());
            var classicLeagueData = await client.GetClassicLeague(leagueId, 1);

            return View(classicLeagueData.Standings.Entries);
        }
    }
}